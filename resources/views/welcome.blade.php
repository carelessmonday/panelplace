@extends('layout.app')

@section('content')
    <div class="container">
        <!-- Heading -->
        <h1 class="page-title uppercase lg:mt-0">Browse Earning</h1>

        <!-- Tab nav -->
        <div class="border-b-2">
            <div class="flex font-light page-tabs -mb-3">
                <a class="flex items-center page-tabs-nav border-b-8 block active" href="#">Community</a>
                <a class="flex items-center page-tabs-nav border-b-8 block" href="#">My Earning Proofs</a>
            </div>
        </div>

        <!-- Stats -->
        <div id="stats" class="flex items-center flex-col py-40px">
            <span class="text-center">4658 Earning Proofs</span>
            <span class="text-center">uploaded by our beloved PanelPlace Community</span>
            <button class="text-white">
                Add My Earning
            </button>
        </div>

        <!-- Featured earnings -->
        <h2 class="page-sub-title uppercase lg:mt-0">Featured Earnings</h2>

        <div> <!-- Tiles row -->
            <div class="justify-between sm:block lg:flex md:block mt-8">
                <div class="earning-tile lg:flex-1 border mb-8">
                    <div class="flex flex-col tile-front">
                        <div class="image-container"
                             style="background-image: url('http://via.placeholder.com/380x214')"></div>
                        <div class="money-value-container rounded-full bg-white border self-end mr-8 flex flex-col justify-center items-center">
                            <span class="block">1000</span>
                            <span class="block">SGD</span>
                        </div>
                        <div class="panel-info-container border-b py-8 mx-8">
                            <span class="block">AIP Surveys</span>
                            <span class="block">(Indonesia)</span>
                        </div>
                        <div class="uploader-info-container m-6 flex content-center">
                            <img class="rounded-full" src="http://via.placeholder.com/50x50">
                            <div class="pl-8 flex flex-col justify-center">
                                <span class="block">Uploaded by</span>
                                <span class="block"><strong>Mathew York</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="tile-back">
                        <div class="flex flex-col justify-between">
                            <p class="earning-comment text-white text-center">
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, eum consequatur
                                suscipit magni porro. Molestias reiciendis"
                            </p>
                            <div class="user-info text-center">
                                <img class="rounded-full p-2" src="http://via.placeholder.com/50x50">
                                <span class="block text-white p-2 text-xl">By <strong>Ricky Kwek on June 31, 2017</strong></span>
                                <div class="text-white flex justify-center items-center p-2 text-xl">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="0.952cm" height="0.459cm">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M26.564,6.643 C26.386,6.823 26.097,6.822 25.920,6.641 C23.497,4.167 20.480,2.514 17.194,1.810 C18.556,2.910 19.432,4.601 19.432,6.499 C19.432,9.807 16.773,12.498 13.506,12.498 C10.239,12.498 7.581,9.807 7.581,6.499 C7.581,4.617 8.442,2.937 9.785,1.835 C6.542,2.554 3.567,4.196 1.171,6.641 L1.067,6.747 C0.978,6.839 0.860,6.885 0.743,6.885 C0.628,6.885 0.511,6.840 0.423,6.750 C0.243,6.571 0.242,6.279 0.420,6.098 L0.525,5.991 C3.976,2.467 8.572,0.520 13.469,0.500 C13.481,0.500 13.494,0.498 13.506,0.498 C13.511,0.498 13.514,0.499 13.519,0.499 C13.528,0.499 13.536,0.498 13.546,0.498 C18.473,0.498 23.097,2.449 26.567,5.991 C26.745,6.173 26.743,6.464 26.564,6.643 ZM13.519,1.422 C13.502,1.422 13.486,1.423 13.469,1.423 C10.721,1.443 8.492,3.711 8.492,6.499 C8.492,9.298 10.741,11.575 13.506,11.575 C16.271,11.575 18.520,9.298 18.520,6.499 C18.520,3.704 16.278,1.429 13.519,1.422 ZM13.506,4.191 C12.249,4.191 11.226,5.226 11.226,6.499 C11.226,6.753 11.023,6.960 10.770,6.960 C10.519,6.960 10.315,6.753 10.315,6.499 C10.315,4.717 11.746,3.268 13.506,3.268 C13.758,3.268 13.962,3.474 13.962,3.728 C13.962,3.984 13.758,4.191 13.506,4.191 Z"/>
                                    </svg>
                                    <span class="bloc;">2500 views</span>
                                </div>
                            </div>
                            <button class="text-white py-8 text-2xl">View Earning</button>
                        </div>
                    </div>
                </div>

                <div class="earning-tile lg:flex-1 lg:mx-8 border mb-8">
                    <div class="flex flex-col tile-front">
                        <div class="image-container"
                             style="background-image: url('http://via.placeholder.com/380x214')"></div>
                        <div class="money-value-container rounded-full bg-white border self-end mr-8 flex flex-col justify-center items-center">
                            <span class="block">1000</span>
                            <span class="block">SGD</span>
                        </div>
                        <div class="panel-info-container border-b py-8 mx-8">
                            <span class="block">AIP Surveys</span>
                            <span class="block">(Indonesia)</span>
                        </div>
                        <div class="uploader-info-container m-6 flex content-center">
                            <img class="rounded-full" src="http://via.placeholder.com/50x50">
                            <div class="pl-8 flex flex-col justify-center">
                                <span class="block">Uploaded by</span>
                                <span class="block"><strong>Mathew York</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="tile-back">
                        <div class="flex flex-col justify-between">
                            <p class="earning-comment text-white text-center">
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, eum consequatur
                                suscipit magni porro. Molestias reiciendis"
                            </p>
                            <div class="user-info text-center">
                                <img class="rounded-full p-2" src="http://via.placeholder.com/50x50">
                                <span class="block text-white p-2 text-xl">By <strong>Ricky Kwek on June 31, 2017</strong></span>
                                <div class="text-white flex justify-center items-center p-2 text-xl">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="0.952cm" height="0.459cm">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M26.564,6.643 C26.386,6.823 26.097,6.822 25.920,6.641 C23.497,4.167 20.480,2.514 17.194,1.810 C18.556,2.910 19.432,4.601 19.432,6.499 C19.432,9.807 16.773,12.498 13.506,12.498 C10.239,12.498 7.581,9.807 7.581,6.499 C7.581,4.617 8.442,2.937 9.785,1.835 C6.542,2.554 3.567,4.196 1.171,6.641 L1.067,6.747 C0.978,6.839 0.860,6.885 0.743,6.885 C0.628,6.885 0.511,6.840 0.423,6.750 C0.243,6.571 0.242,6.279 0.420,6.098 L0.525,5.991 C3.976,2.467 8.572,0.520 13.469,0.500 C13.481,0.500 13.494,0.498 13.506,0.498 C13.511,0.498 13.514,0.499 13.519,0.499 C13.528,0.499 13.536,0.498 13.546,0.498 C18.473,0.498 23.097,2.449 26.567,5.991 C26.745,6.173 26.743,6.464 26.564,6.643 ZM13.519,1.422 C13.502,1.422 13.486,1.423 13.469,1.423 C10.721,1.443 8.492,3.711 8.492,6.499 C8.492,9.298 10.741,11.575 13.506,11.575 C16.271,11.575 18.520,9.298 18.520,6.499 C18.520,3.704 16.278,1.429 13.519,1.422 ZM13.506,4.191 C12.249,4.191 11.226,5.226 11.226,6.499 C11.226,6.753 11.023,6.960 10.770,6.960 C10.519,6.960 10.315,6.753 10.315,6.499 C10.315,4.717 11.746,3.268 13.506,3.268 C13.758,3.268 13.962,3.474 13.962,3.728 C13.962,3.984 13.758,4.191 13.506,4.191 Z"/>
                                    </svg>
                                    <span class="bloc;">2500 views</span>
                                </div>
                            </div>
                            <button class="text-white py-8 text-2xl">View Earning</button>
                        </div>
                    </div>
                </div>

                <div class="earning-tile lg:flex-1 border mb-8">
                    <div class="flex flex-col tile-front">
                        <div class="image-container"
                             style="background-image: url('http://via.placeholder.com/380x214')"></div>
                        <div class="money-value-container rounded-full bg-white border self-end mr-8 flex flex-col justify-center items-center">
                            <span class="block">1000</span>
                            <span class="block">SGD</span>
                        </div>
                        <div class="panel-info-container border-b py-8 mx-8">
                            <span class="block">AIP Surveys</span>
                            <span class="block">(Indonesia)</span>
                        </div>
                        <div class="uploader-info-container m-6 flex content-center">
                            <img class="rounded-full" src="http://via.placeholder.com/50x50">
                            <div class="pl-8 flex flex-col justify-center">
                                <span class="block">Uploaded by</span>
                                <span class="block"><strong>Mathew York</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="tile-back">
                        <div class="flex flex-col justify-between">
                            <p class="earning-comment text-white text-center">
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, eum consequatur
                                suscipit magni porro. Molestias reiciendis"
                            </p>
                            <div class="user-info text-center">
                                <img class="rounded-full p-2" src="http://via.placeholder.com/50x50">
                                <span class="block text-white p-2 text-xl">By <strong>Ricky Kwek on June 31, 2017</strong></span>
                                <div class="text-white flex justify-center items-center p-2 text-xl">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="0.952cm" height="0.459cm">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M26.564,6.643 C26.386,6.823 26.097,6.822 25.920,6.641 C23.497,4.167 20.480,2.514 17.194,1.810 C18.556,2.910 19.432,4.601 19.432,6.499 C19.432,9.807 16.773,12.498 13.506,12.498 C10.239,12.498 7.581,9.807 7.581,6.499 C7.581,4.617 8.442,2.937 9.785,1.835 C6.542,2.554 3.567,4.196 1.171,6.641 L1.067,6.747 C0.978,6.839 0.860,6.885 0.743,6.885 C0.628,6.885 0.511,6.840 0.423,6.750 C0.243,6.571 0.242,6.279 0.420,6.098 L0.525,5.991 C3.976,2.467 8.572,0.520 13.469,0.500 C13.481,0.500 13.494,0.498 13.506,0.498 C13.511,0.498 13.514,0.499 13.519,0.499 C13.528,0.499 13.536,0.498 13.546,0.498 C18.473,0.498 23.097,2.449 26.567,5.991 C26.745,6.173 26.743,6.464 26.564,6.643 ZM13.519,1.422 C13.502,1.422 13.486,1.423 13.469,1.423 C10.721,1.443 8.492,3.711 8.492,6.499 C8.492,9.298 10.741,11.575 13.506,11.575 C16.271,11.575 18.520,9.298 18.520,6.499 C18.520,3.704 16.278,1.429 13.519,1.422 ZM13.506,4.191 C12.249,4.191 11.226,5.226 11.226,6.499 C11.226,6.753 11.023,6.960 10.770,6.960 C10.519,6.960 10.315,6.753 10.315,6.499 C10.315,4.717 11.746,3.268 13.506,3.268 C13.758,3.268 13.962,3.474 13.962,3.728 C13.962,3.984 13.758,4.191 13.506,4.191 Z"/>
                                    </svg>
                                    <span class="bloc;">2500 views</span>
                                </div>
                            </div>
                            <button class="text-white py-8 text-2xl">View Earning</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Slider Nav -->
        <div id="slider-nav" class="flex items-center justify-center mb-8">
            <a class="block active mx-1 my-8" href="#"></a>
            <a class="block mx-1 my-8" href="#"></a>
            <a class="block mx-1 my-8" href="#"></a>
        </div>

        <!-- All Community Earnings -->
        <div class="lg:flex md:flex justify-between items-center mt-8 mb-8">
            <h2 class="uppercase page-sub-title m-0">All Community Earnings</h2>
            <div>
                <form>
                    <select class="border border-grey-dark text-grey-dark px-4 py-2 uppercase bg-transparent">
                        <option>All Countries</option>
                        <option>Singapore</option>
                        <option>Malaysia</option>
                        <option>Indonesia</option>
                    </select>
                    <select class="border border-grey-dark text-grey-dark px-4 py-2 uppercase bg-transparent lg:mx-4 md:mx-4">
                        <option>All Survey Panels</option>
                        <option>AIP Surveys</option>
                        <option>YouGov</option>
                        <option>Valued Opinions</option>
                    </select>
                    <select class="border border-grey-dark text-grey-dark px-4 py-2 uppercase bg-transparent">
                        <option>Sort By</option>
                        <option>Value</option>
                        <option>Survey Panel</option>
                        <option>Country</option>
                    </select>
                </form>
            </div>
        </div>

        <div> <!-- Tiles row -->
            <div class="justify-between sm:block lg:flex md:block mt-8">
                <div class="earning-tile lg:flex-1 border mb-8">
                    <div class="flex flex-col tile-front">
                        <div class="image-container"
                             style="background-image: url('http://via.placeholder.com/380x214')"></div>
                        <div class="money-value-container rounded-full bg-white border self-end mr-8 flex flex-col justify-center items-center">
                            <span class="block">1000</span>
                            <span class="block">SGD</span>
                        </div>
                        <div class="panel-info-container border-b py-8 mx-8">
                            <span class="block">AIP Surveys</span>
                            <span class="block">(Indonesia)</span>
                        </div>
                        <div class="uploader-info-container m-6 flex content-center">
                            <img class="rounded-full" src="http://via.placeholder.com/50x50">
                            <div class="pl-8 flex flex-col justify-center">
                                <span class="block">Uploaded by</span>
                                <span class="block"><strong>Mathew York</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="tile-back">
                        <div class="flex flex-col justify-between">
                            <p class="earning-comment text-white text-center">
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, eum consequatur
                                suscipit magni porro. Molestias reiciendis"
                            </p>
                            <div class="user-info text-center">
                                <img class="rounded-full p-2" src="http://via.placeholder.com/50x50">
                                <span class="block text-white p-2 text-xl">By <strong>Ricky Kwek on June 31, 2017</strong></span>
                                <div class="text-white flex justify-center items-center p-2 text-xl">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="0.952cm" height="0.459cm">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M26.564,6.643 C26.386,6.823 26.097,6.822 25.920,6.641 C23.497,4.167 20.480,2.514 17.194,1.810 C18.556,2.910 19.432,4.601 19.432,6.499 C19.432,9.807 16.773,12.498 13.506,12.498 C10.239,12.498 7.581,9.807 7.581,6.499 C7.581,4.617 8.442,2.937 9.785,1.835 C6.542,2.554 3.567,4.196 1.171,6.641 L1.067,6.747 C0.978,6.839 0.860,6.885 0.743,6.885 C0.628,6.885 0.511,6.840 0.423,6.750 C0.243,6.571 0.242,6.279 0.420,6.098 L0.525,5.991 C3.976,2.467 8.572,0.520 13.469,0.500 C13.481,0.500 13.494,0.498 13.506,0.498 C13.511,0.498 13.514,0.499 13.519,0.499 C13.528,0.499 13.536,0.498 13.546,0.498 C18.473,0.498 23.097,2.449 26.567,5.991 C26.745,6.173 26.743,6.464 26.564,6.643 ZM13.519,1.422 C13.502,1.422 13.486,1.423 13.469,1.423 C10.721,1.443 8.492,3.711 8.492,6.499 C8.492,9.298 10.741,11.575 13.506,11.575 C16.271,11.575 18.520,9.298 18.520,6.499 C18.520,3.704 16.278,1.429 13.519,1.422 ZM13.506,4.191 C12.249,4.191 11.226,5.226 11.226,6.499 C11.226,6.753 11.023,6.960 10.770,6.960 C10.519,6.960 10.315,6.753 10.315,6.499 C10.315,4.717 11.746,3.268 13.506,3.268 C13.758,3.268 13.962,3.474 13.962,3.728 C13.962,3.984 13.758,4.191 13.506,4.191 Z"/>
                                    </svg>
                                    <span class="bloc;">2500 views</span>
                                </div>
                            </div>
                            <button class="text-white py-8 text-2xl">View Earning</button>
                        </div>
                    </div>
                </div>

                <div class="earning-tile lg:flex-1 lg:mx-8 border mb-8">
                    <div class="flex flex-col tile-front">
                        <div class="image-container"
                             style="background-image: url('http://via.placeholder.com/380x214')"></div>
                        <div class="money-value-container rounded-full bg-white border self-end mr-8 flex flex-col justify-center items-center">
                            <span class="block">1000</span>
                            <span class="block">SGD</span>
                        </div>
                        <div class="panel-info-container border-b py-8 mx-8">
                            <span class="block">AIP Surveys</span>
                            <span class="block">(Indonesia)</span>
                        </div>
                        <div class="uploader-info-container m-6 flex content-center">
                            <img class="rounded-full" src="http://via.placeholder.com/50x50">
                            <div class="pl-8 flex flex-col justify-center">
                                <span class="block">Uploaded by</span>
                                <span class="block"><strong>Mathew York</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="tile-back">
                        <div class="flex flex-col justify-between">
                            <p class="earning-comment text-white text-center">
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, eum consequatur
                                suscipit magni porro. Molestias reiciendis"
                            </p>
                            <div class="user-info text-center">
                                <img class="rounded-full p-2" src="http://via.placeholder.com/50x50">
                                <span class="block text-white p-2 text-xl">By <strong>Ricky Kwek on June 31, 2017</strong></span>
                                <div class="text-white flex justify-center items-center p-2 text-xl">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="0.952cm" height="0.459cm">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M26.564,6.643 C26.386,6.823 26.097,6.822 25.920,6.641 C23.497,4.167 20.480,2.514 17.194,1.810 C18.556,2.910 19.432,4.601 19.432,6.499 C19.432,9.807 16.773,12.498 13.506,12.498 C10.239,12.498 7.581,9.807 7.581,6.499 C7.581,4.617 8.442,2.937 9.785,1.835 C6.542,2.554 3.567,4.196 1.171,6.641 L1.067,6.747 C0.978,6.839 0.860,6.885 0.743,6.885 C0.628,6.885 0.511,6.840 0.423,6.750 C0.243,6.571 0.242,6.279 0.420,6.098 L0.525,5.991 C3.976,2.467 8.572,0.520 13.469,0.500 C13.481,0.500 13.494,0.498 13.506,0.498 C13.511,0.498 13.514,0.499 13.519,0.499 C13.528,0.499 13.536,0.498 13.546,0.498 C18.473,0.498 23.097,2.449 26.567,5.991 C26.745,6.173 26.743,6.464 26.564,6.643 ZM13.519,1.422 C13.502,1.422 13.486,1.423 13.469,1.423 C10.721,1.443 8.492,3.711 8.492,6.499 C8.492,9.298 10.741,11.575 13.506,11.575 C16.271,11.575 18.520,9.298 18.520,6.499 C18.520,3.704 16.278,1.429 13.519,1.422 ZM13.506,4.191 C12.249,4.191 11.226,5.226 11.226,6.499 C11.226,6.753 11.023,6.960 10.770,6.960 C10.519,6.960 10.315,6.753 10.315,6.499 C10.315,4.717 11.746,3.268 13.506,3.268 C13.758,3.268 13.962,3.474 13.962,3.728 C13.962,3.984 13.758,4.191 13.506,4.191 Z"/>
                                    </svg>
                                    <span class="bloc;">2500 views</span>
                                </div>
                            </div>
                            <button class="text-white py-8 text-2xl">View Earning</button>
                        </div>
                    </div>
                </div>

                <div class="earning-tile lg:flex-1 border mb-8">
                    <div class="flex flex-col tile-front">
                        <div class="image-container"
                             style="background-image: url('http://via.placeholder.com/380x214')"></div>
                        <div class="money-value-container rounded-full bg-white border self-end mr-8 flex flex-col justify-center items-center">
                            <span class="block">1000</span>
                            <span class="block">SGD</span>
                        </div>
                        <div class="panel-info-container border-b py-8 mx-8">
                            <span class="block">AIP Surveys</span>
                            <span class="block">(Indonesia)</span>
                        </div>
                        <div class="uploader-info-container m-6 flex content-center">
                            <img class="rounded-full" src="http://via.placeholder.com/50x50">
                            <div class="pl-8 flex flex-col justify-center">
                                <span class="block">Uploaded by</span>
                                <span class="block"><strong>Mathew York</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="tile-back">
                        <div class="flex flex-col justify-between">
                            <p class="earning-comment text-white text-center">
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, eum consequatur
                                suscipit magni porro. Molestias reiciendis"
                            </p>
                            <div class="user-info text-center">
                                <img class="rounded-full p-2" src="http://via.placeholder.com/50x50">
                                <span class="block text-white p-2 text-xl">By <strong>Ricky Kwek on June 31, 2017</strong></span>
                                <div class="text-white flex justify-center items-center p-2 text-xl">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="0.952cm" height="0.459cm">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M26.564,6.643 C26.386,6.823 26.097,6.822 25.920,6.641 C23.497,4.167 20.480,2.514 17.194,1.810 C18.556,2.910 19.432,4.601 19.432,6.499 C19.432,9.807 16.773,12.498 13.506,12.498 C10.239,12.498 7.581,9.807 7.581,6.499 C7.581,4.617 8.442,2.937 9.785,1.835 C6.542,2.554 3.567,4.196 1.171,6.641 L1.067,6.747 C0.978,6.839 0.860,6.885 0.743,6.885 C0.628,6.885 0.511,6.840 0.423,6.750 C0.243,6.571 0.242,6.279 0.420,6.098 L0.525,5.991 C3.976,2.467 8.572,0.520 13.469,0.500 C13.481,0.500 13.494,0.498 13.506,0.498 C13.511,0.498 13.514,0.499 13.519,0.499 C13.528,0.499 13.536,0.498 13.546,0.498 C18.473,0.498 23.097,2.449 26.567,5.991 C26.745,6.173 26.743,6.464 26.564,6.643 ZM13.519,1.422 C13.502,1.422 13.486,1.423 13.469,1.423 C10.721,1.443 8.492,3.711 8.492,6.499 C8.492,9.298 10.741,11.575 13.506,11.575 C16.271,11.575 18.520,9.298 18.520,6.499 C18.520,3.704 16.278,1.429 13.519,1.422 ZM13.506,4.191 C12.249,4.191 11.226,5.226 11.226,6.499 C11.226,6.753 11.023,6.960 10.770,6.960 C10.519,6.960 10.315,6.753 10.315,6.499 C10.315,4.717 11.746,3.268 13.506,3.268 C13.758,3.268 13.962,3.474 13.962,3.728 C13.962,3.984 13.758,4.191 13.506,4.191 Z"/>
                                    </svg>
                                    <span class="bloc;">2500 views</span>
                                </div>
                            </div>
                            <button class="text-white py-8 text-2xl">View Earning</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div> <!-- Tiles row -->
            <div class="justify-between sm:block lg:flex md:block mt-8">
                <div class="earning-tile lg:flex-1 border mb-8">
                    <div class="flex flex-col tile-front">
                        <div class="image-container"
                             style="background-image: url('http://via.placeholder.com/380x214')"></div>
                        <div class="money-value-container rounded-full bg-white border self-end mr-8 flex flex-col justify-center items-center">
                            <span class="block">1000</span>
                            <span class="block">SGD</span>
                        </div>
                        <div class="panel-info-container border-b py-8 mx-8">
                            <span class="block">AIP Surveys</span>
                            <span class="block">(Indonesia)</span>
                        </div>
                        <div class="uploader-info-container m-6 flex content-center">
                            <img class="rounded-full" src="http://via.placeholder.com/50x50">
                            <div class="pl-8 flex flex-col justify-center">
                                <span class="block">Uploaded by</span>
                                <span class="block"><strong>Mathew York</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="tile-back">
                        <div class="flex flex-col justify-between">
                            <p class="earning-comment text-white text-center">
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, eum consequatur
                                suscipit magni porro. Molestias reiciendis"
                            </p>
                            <div class="user-info text-center">
                                <img class="rounded-full p-2" src="http://via.placeholder.com/50x50">
                                <span class="block text-white p-2 text-xl">By <strong>Ricky Kwek on June 31, 2017</strong></span>
                                <div class="text-white flex justify-center items-center p-2 text-xl">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="0.952cm" height="0.459cm">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M26.564,6.643 C26.386,6.823 26.097,6.822 25.920,6.641 C23.497,4.167 20.480,2.514 17.194,1.810 C18.556,2.910 19.432,4.601 19.432,6.499 C19.432,9.807 16.773,12.498 13.506,12.498 C10.239,12.498 7.581,9.807 7.581,6.499 C7.581,4.617 8.442,2.937 9.785,1.835 C6.542,2.554 3.567,4.196 1.171,6.641 L1.067,6.747 C0.978,6.839 0.860,6.885 0.743,6.885 C0.628,6.885 0.511,6.840 0.423,6.750 C0.243,6.571 0.242,6.279 0.420,6.098 L0.525,5.991 C3.976,2.467 8.572,0.520 13.469,0.500 C13.481,0.500 13.494,0.498 13.506,0.498 C13.511,0.498 13.514,0.499 13.519,0.499 C13.528,0.499 13.536,0.498 13.546,0.498 C18.473,0.498 23.097,2.449 26.567,5.991 C26.745,6.173 26.743,6.464 26.564,6.643 ZM13.519,1.422 C13.502,1.422 13.486,1.423 13.469,1.423 C10.721,1.443 8.492,3.711 8.492,6.499 C8.492,9.298 10.741,11.575 13.506,11.575 C16.271,11.575 18.520,9.298 18.520,6.499 C18.520,3.704 16.278,1.429 13.519,1.422 ZM13.506,4.191 C12.249,4.191 11.226,5.226 11.226,6.499 C11.226,6.753 11.023,6.960 10.770,6.960 C10.519,6.960 10.315,6.753 10.315,6.499 C10.315,4.717 11.746,3.268 13.506,3.268 C13.758,3.268 13.962,3.474 13.962,3.728 C13.962,3.984 13.758,4.191 13.506,4.191 Z"/>
                                    </svg>
                                    <span class="bloc;">2500 views</span>
                                </div>
                            </div>
                            <button class="text-white py-8 text-2xl">View Earning</button>
                        </div>
                    </div>
                </div>

                <div class="earning-tile lg:flex-1 lg:mx-8 border mb-8">
                    <div class="flex flex-col tile-front">
                        <div class="image-container"
                             style="background-image: url('http://via.placeholder.com/380x214')"></div>
                        <div class="money-value-container rounded-full bg-white border self-end mr-8 flex flex-col justify-center items-center">
                            <span class="block">1000</span>
                            <span class="block">SGD</span>
                        </div>
                        <div class="panel-info-container border-b py-8 mx-8">
                            <span class="block">AIP Surveys</span>
                            <span class="block">(Indonesia)</span>
                        </div>
                        <div class="uploader-info-container m-6 flex content-center">
                            <img class="rounded-full" src="http://via.placeholder.com/50x50">
                            <div class="pl-8 flex flex-col justify-center">
                                <span class="block">Uploaded by</span>
                                <span class="block"><strong>Mathew York</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="tile-back">
                        <div class="flex flex-col justify-between">
                            <p class="earning-comment text-white text-center">
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, eum consequatur
                                suscipit magni porro. Molestias reiciendis"
                            </p>
                            <div class="user-info text-center">
                                <img class="rounded-full p-2" src="http://via.placeholder.com/50x50">
                                <span class="block text-white p-2 text-xl">By <strong>Ricky Kwek on June 31, 2017</strong></span>
                                <div class="text-white flex justify-center items-center p-2 text-xl">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="0.952cm" height="0.459cm">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M26.564,6.643 C26.386,6.823 26.097,6.822 25.920,6.641 C23.497,4.167 20.480,2.514 17.194,1.810 C18.556,2.910 19.432,4.601 19.432,6.499 C19.432,9.807 16.773,12.498 13.506,12.498 C10.239,12.498 7.581,9.807 7.581,6.499 C7.581,4.617 8.442,2.937 9.785,1.835 C6.542,2.554 3.567,4.196 1.171,6.641 L1.067,6.747 C0.978,6.839 0.860,6.885 0.743,6.885 C0.628,6.885 0.511,6.840 0.423,6.750 C0.243,6.571 0.242,6.279 0.420,6.098 L0.525,5.991 C3.976,2.467 8.572,0.520 13.469,0.500 C13.481,0.500 13.494,0.498 13.506,0.498 C13.511,0.498 13.514,0.499 13.519,0.499 C13.528,0.499 13.536,0.498 13.546,0.498 C18.473,0.498 23.097,2.449 26.567,5.991 C26.745,6.173 26.743,6.464 26.564,6.643 ZM13.519,1.422 C13.502,1.422 13.486,1.423 13.469,1.423 C10.721,1.443 8.492,3.711 8.492,6.499 C8.492,9.298 10.741,11.575 13.506,11.575 C16.271,11.575 18.520,9.298 18.520,6.499 C18.520,3.704 16.278,1.429 13.519,1.422 ZM13.506,4.191 C12.249,4.191 11.226,5.226 11.226,6.499 C11.226,6.753 11.023,6.960 10.770,6.960 C10.519,6.960 10.315,6.753 10.315,6.499 C10.315,4.717 11.746,3.268 13.506,3.268 C13.758,3.268 13.962,3.474 13.962,3.728 C13.962,3.984 13.758,4.191 13.506,4.191 Z"/>
                                    </svg>
                                    <span class="bloc;">2500 views</span>
                                </div>
                            </div>
                            <button class="text-white py-8 text-2xl">View Earning</button>
                        </div>
                    </div>
                </div>

                <div class="earning-tile lg:flex-1 border mb-8">
                    <div class="flex flex-col tile-front">
                        <div class="image-container"
                             style="background-image: url('http://via.placeholder.com/380x214')"></div>
                        <div class="money-value-container rounded-full bg-white border self-end mr-8 flex flex-col justify-center items-center">
                            <span class="block">1000</span>
                            <span class="block">SGD</span>
                        </div>
                        <div class="panel-info-container border-b py-8 mx-8">
                            <span class="block">AIP Surveys</span>
                            <span class="block">(Indonesia)</span>
                        </div>
                        <div class="uploader-info-container m-6 flex content-center">
                            <img class="rounded-full" src="http://via.placeholder.com/50x50">
                            <div class="pl-8 flex flex-col justify-center">
                                <span class="block">Uploaded by</span>
                                <span class="block"><strong>Mathew York</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="tile-back">
                        <div class="flex flex-col justify-between">
                            <p class="earning-comment text-white text-center">
                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, eum consequatur
                                suscipit magni porro. Molestias reiciendis"
                            </p>
                            <div class="user-info text-center">
                                <img class="rounded-full p-2" src="http://via.placeholder.com/50x50">
                                <span class="block text-white p-2 text-xl">By <strong>Ricky Kwek on June 31, 2017</strong></span>
                                <div class="text-white flex justify-center items-center p-2 text-xl">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="0.952cm" height="0.459cm">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M26.564,6.643 C26.386,6.823 26.097,6.822 25.920,6.641 C23.497,4.167 20.480,2.514 17.194,1.810 C18.556,2.910 19.432,4.601 19.432,6.499 C19.432,9.807 16.773,12.498 13.506,12.498 C10.239,12.498 7.581,9.807 7.581,6.499 C7.581,4.617 8.442,2.937 9.785,1.835 C6.542,2.554 3.567,4.196 1.171,6.641 L1.067,6.747 C0.978,6.839 0.860,6.885 0.743,6.885 C0.628,6.885 0.511,6.840 0.423,6.750 C0.243,6.571 0.242,6.279 0.420,6.098 L0.525,5.991 C3.976,2.467 8.572,0.520 13.469,0.500 C13.481,0.500 13.494,0.498 13.506,0.498 C13.511,0.498 13.514,0.499 13.519,0.499 C13.528,0.499 13.536,0.498 13.546,0.498 C18.473,0.498 23.097,2.449 26.567,5.991 C26.745,6.173 26.743,6.464 26.564,6.643 ZM13.519,1.422 C13.502,1.422 13.486,1.423 13.469,1.423 C10.721,1.443 8.492,3.711 8.492,6.499 C8.492,9.298 10.741,11.575 13.506,11.575 C16.271,11.575 18.520,9.298 18.520,6.499 C18.520,3.704 16.278,1.429 13.519,1.422 ZM13.506,4.191 C12.249,4.191 11.226,5.226 11.226,6.499 C11.226,6.753 11.023,6.960 10.770,6.960 C10.519,6.960 10.315,6.753 10.315,6.499 C10.315,4.717 11.746,3.268 13.506,3.268 C13.758,3.268 13.962,3.474 13.962,3.728 C13.962,3.984 13.758,4.191 13.506,4.191 Z"/>
                                    </svg>
                                    <span class="bloc;">2500 views</span>
                                </div>
                            </div>
                            <button class="text-white py-8 text-2xl">View Earning</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
