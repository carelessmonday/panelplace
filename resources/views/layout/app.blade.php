<!DOCTYPE html>
<!-- saved from url=(0046)http://staging.panelplace.com/member/dashboard -->
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="SgMF4SWBXHuDjLuee9LZkg6FWw3nskM112FjzwiI">
        <title>Dashboard | PanelPlace</title>
        <title></title>
        <link rel="shortcut icon" href="http://staging.panelplace.com/images/favicon.ico" type="image/vnd.microsoft.icon">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ url('css/app.css') }}">
        <link rel="stylesheet" href="{{ url('css/app-fcec25b251.css') }}">
        <script type="text/javascript" async="" src="./Dashboard _ PanelPlace_files/request"></script>
    </head>
    <body data-spy="scroll" data-target=".tabpanelplace" data-offset="50">
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header"><button type="button" data-toggle="collapse" data-target="#app-navbar-collapse" class="navbar-toggle collapsed"><span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a   class="navbar-brand"><img src="http://staging.panelplace.com/panelplace/images/site/pp-logo.png"></a></div>
                    <div id="app-navbar-collapse" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-left"></ul>
                        <ul id="navbar-menu-desktop" class="nav navbar-nav navbar-right">
                            <li class="help-support"><a id="help-support" target="_blank">Help &amp; Support</a></li>
                            <li class="dropdown">
                                <a   data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle profile-picture"><img src="http://staging.panelplace.com/panelplace/images/site/pp-logo.png" alt="Profile Image" class="profile-image-thumnail">
                                Heriyanto <span class="caret"></span></a> 
                                <ul role="menu" class="dropdown-menu">
                                    <li><a  >Profile</a></li>
                                    <li>
                                        <a>
                                        Logout
                                        </a> 
                                        <form id="logout-form" action="#" method="POST" style="display: none;"><input type="hidden" name="_token" value="SgMF4SWBXHuDjLuee9LZkg6FWw3nskM112FjzwiI"></form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav class="primary-menu">
                <div class="container">
                    <ul>
                        <li class="active"><a target="_parent">Dashboard</a></li>
                        <li><a>Panel Store</a></li>
                        <li><a >Earnings</a></li>
                        <li><a>Promotions</a></li>
                    </ul>
                </div>
            </nav>
            <div class="page-container">
            	@yield('content')
            </div>
        </div>
        <!-- Scripts -->
       
    </body>
</html>