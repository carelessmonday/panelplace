<?php

namespace App\Http\Controllers;

use App\PprfImage;
use Illuminate\Http\Request;

class CommunityEarningProofController extends Controller
{

    public function show()
    {
        $proofs = PprfImage::where(['isPublished' => 1])->get();

        return view('proofs', compact('proofs'));
    }

}
