<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PprfImage extends Model
{
    protected $dates = [
        'uploadedDate'
    ];
}
